<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/tes', function () {
	Mail::send([],[],function($message){
		$message->to('giarsyani.nuli@gmail.com','Nuli Giarsyani')->subject('New Account');
	});
});
// Auth::routes();

Route::get('/report', 'HomeController@report')->name('report');
Route::post('/postLapor', 'HomeController@postLapor')->name('post.lapor');
Route::get('/masyarakat', 'HomeController@index')->name('home');
Route::post('/sendComment', 'HomeController@sendComment')->name('comment');
