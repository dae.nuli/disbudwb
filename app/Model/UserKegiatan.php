<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserKegiatan extends Model
{
    // protected $connection = 'mysql2';
    protected $table = 'user_kegiatan';

    public function user()
    {
    	return $this->belongsTo(KpaUser::class, 'id_user');
    }

    public function program()
    {
    	return $this->belongsTo(Program::class, 'id_program');
    }

    public function kegiatan()
    {
    	return $this->belongsTo(Kegiatan::class, 'id_kegiatan');
    }
}
