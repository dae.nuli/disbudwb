<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserProgram extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'user_program';
}
