<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Complain extends Model
{
    protected $table = 'complains';
    protected $fillable = ['skpd_id','program_id','kegiatan_id','name','email','phone_number','subject','content'];
}
