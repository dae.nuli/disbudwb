<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\UserKegiatan;
use App\Model\KpaUser;
use App\Model\Program;
use App\Model\Kegiatan;
use App\Model\Complain;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $kegiatan = UserKegiatan::whereHas('user', function ($query) {
                                $query->where('is_active', '1');
                            })->orderBy('id', 'asc');
        $idSkpd = $request->skpd;
        $idProgram = $request->program;
        $idKegiatan = $request->kegiatan;
        if (!empty($idSkpd)) {
            $kegiatan = $kegiatan->where('id_user', $idSkpd);
        }
        if (!empty($idProgram)) {
            $kegiatan = $kegiatan->where('id_program', $idProgram);
            $listKegiatan = Kegiatan::where('id_program', $idProgram)->orderBy('id', 'desc')->get();
        }
        if (!empty($idKegiatan)) {
            $kegiatan = $kegiatan->where('id_kegiatan', $idKegiatan);
        }
        $kegiatan = $kegiatan->get();

        $skpd = KpaUser::where('is_active', '1')->get();
        $program = Program::orderBy('id', 'desc')->get();
        return view('masyarakat.home', compact('kegiatan', 'skpd', 'program', 'idSkpd', 'idProgram', 'idKegiatan', 'listKegiatan'));
    }

    public function sendComment(Request $request)
    {
        $created = Complain::create([
            'skpd_id' => $request->skpd,
            'program_id' => $request->program,
            'kegiatan_id' => $request->kegiatan,
            'name' => $request->name,
            'email' => $request->email,
            'phone_number' => $request->phone,
            'content' => $request->comment,
        ]);

        if ($request->ajax()) {
            if ($created->exists) {
                return response()->json(['status' => true, 'msg' => '<div class="alert alert-success">Komentar terkirim</div>']);
            } else {
                return response()->json(['status' => false, 'msg' => '<div class="alert alert-success">Komentar tidak terkirim</div>']);
            }
        } else {
            return redirect('masyarakat')->with('status', 'Komentar terkirim');
        }
    }

    public function report(Request $request)
    {
        $data['skpd'] = KpaUser::where('is_active', '1')->get();
        return view('masyarakat.report_skpd', $data);
    }

    public function postLapor(Request $request)
    {

        $this->validate($request, [
            'name'  => 'required', 
            'email' => 'required|email', 
            'phone_number' => 'required|numeric', 
            'skpd' => 'required', 
            'subject' => 'required', 
            'content' => 'required'
        ]);

        // $val = $request->attachment;
        // if(isset($val)) {
        //     $directory = base_path('public/attachment');
        //     $name      = rand(111,9999999).'.'.$val->extension();
        //     if(!File::isDirectory($directory)){
        //         File::makeDirectory($directory, 0777);    
        //     }
        //     $val->move($directory, $name);
            
        //     $request->merge(['attach' => $name]);
        // }else{
        //     unset($request['attach']);
        // }
        // $request->merge(['years' => date('Y')]);

        Complain::create([
            'skpd_id' => $request->skpd,
            'name' => $request->name,
            'email' => $request->email,
            'phone_number' => $request->phone_number,
            'subject' => $request->subject,
            'content' => $request->content
        ]);

        return redirect()->back()->with('success','Terimakasih, Pengaduan anda telah terkirim');

    }
}