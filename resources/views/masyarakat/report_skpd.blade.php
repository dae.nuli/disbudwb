@extends('masyarakat.app')

@section('content')

<div class="container">
    <div class="row">

        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Lapor SKPD</div>

                <div class="panel-body">

                    @if(count($errors))
                    <div class="alert alert-danger alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                      </ul>
                    </div>
                    @endif
                    @if(session()->has('success'))
                    <div class="alert alert-success alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      {{session()->get('success')}}.
                    </div>
                    @else
                    <form action="{{ url('postLapor') }}" method="POST" class="form-horizontal">
                      {{ csrf_field() }}
                      <div class="form-group">
                        <label for="name" class="col-sm-3 control-label">Nama</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" name="name" id="name" autocomplete="off" data-validation="required" value="{{old('name')}}">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="email" class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-8">
                          <input type="email" class="form-control" name="email" id="email" autocomplete="off" data-validation="email" value="{{old('email')}}">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="phone" class="col-sm-3 control-label">Nomor Handphone</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" name="phone_number" id="phone" autocomplete="off" data-validation="number" value="{{old('phone_number')}}">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="skpd" class="col-sm-3 control-label">SKPD</label>
                        <div class="col-sm-8">
                            <select class="form-control dropdown" id="skpd" name="skpd" autocomplete="off" data-validation="required">
                                <option value="">- Pilih SKPD -</option>
                                @foreach($skpd as $row)
                                    <option value="{{$row->id}}">{{$row->instansi_name}}</option>
                                @endforeach
                            </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="subject" class="col-sm-3 control-label">Judul Pengaduan</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" name="subject" id="subject" autocomplete="off" data-validation="required" value="{{old('subject')}}">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="content" class="col-sm-3 control-label">Isi Pengaduan</label>
                        <div class="col-sm-8">
                          <textarea class="form-control" id="content" name="content" rows="5" autocomplete="off" data-validation="required">{{old('content')}}</textarea>
                        </div>
                      </div>
                      {{-- <div class="form-group">
                        <label for="exampleInputFile" class="col-sm-3 control-label">File</label>
                        <div class="col-sm-8">
                          <input type="file" name="attachment" id="exampleInputFile">
                        </div>
                      </div> --}}

                      <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-8">
                          <button type="submit" class="btn btn-success">Kirim</button>
                        </div>
                      </div>
                      </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.full.js"></script>
<script type="text/javascript" src="{{ asset('js/jquery.form-validator.min.js') }}"></script>
<script type="text/javascript">
  
$( ".dropdown" ).select2({
    theme: "bootstrap"
});
$.validate({
    form : '.form-horizontal'
});
</script>
@endsection