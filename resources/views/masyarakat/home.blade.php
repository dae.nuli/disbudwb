@extends('masyarakat.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Pengaduan Masyarakat</div>

                <div class="panel-body">

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div id="ajaxResponse"></div>
                    <form action="{{ url('masyarakat') }}" method="GET">
                        <div class="form-group">
                      <div class="row">
                          <div class="col-md-4">
                              <label class="sr-only" for="skpd">SKPD</label>
                              <select class="form-control dropdown" id="skpd" name="skpd" onchange="this.form.submit()">
                                {{-- <input type="hidden" name="kegiatan" value="0"> --}}
                                {{-- <input type="hidden" name="program" value="0"> --}}
                                <option value="0" selected="">- Semua SKPD -</option>
                                @foreach($skpd as $val)
                                    <option value="{{$val->id}}" {{ ($idSkpd == $val->id) ? 'selected' : '' }}>{{$val->instansi_name}}</option>
                                @endforeach
                              </select>
                          </div>
                        {{-- </div> --}}
                          <div class="col-md-4">
                          {{-- <div class="form-group"> --}}
                              <label class="sr-only" for="program">PROGRAM</label>
                              <select class="form-control dropdown" id="program" name="program" onchange="this.form.submit()">
                                <option value="0" selected="">- Semua Program -</option>
                                @foreach($program as $prog)
                                    <option value="{{$prog->id}}" {{ ($idProgram == $prog->id) ? 'selected' : '' }}>{{$prog->name}}</option>
                                @endforeach
                              </select>
                          {{-- </div> --}}
                          </div>
                          <div class="col-md-4">
                          {{-- <div class="form-group"> --}}
                              <label class="sr-only" for="kegiatan">KEGIATAN</label>
                              <select class="form-control dropdown" id="kegiatan" name="kegiatan" onchange="this.form.submit()">
                                <option value="0" selected="">- Semua Kegiatan -</option>
                                @if(isset($listKegiatan))
                                  @foreach($listKegiatan as $value)
                                      <option value="{{$value->id}}" {{ ($idKegiatan == $value->id) ? 'selected' : '' }}>{{$value->name}}</option>
                                  @endforeach
                                @endif
                              </select>
                          </div>
                        </div>
                      </div>
                    </form>
                    <hr>
                    <table class="table table-bordered">
                        <tr>
                            <th>SKPD</th>
                            <th>PROGRAM</th>
                            <th>KEGIATAN</th>
                            <th>PAGU KEGIATAN</th>
                            <th>USULAN</th>
                        </tr>
                        @foreach($kegiatan as $row)
                        <tr>
                            <td data-skpd="{{$row->id_user}}" data-nama="{{$row->user->instansi_name}}" data-program="{{$row->id_program}}" data-strpro="{{$row->program->name}}" data-kegiatan="{{$row->id_kegiatan}}" data-strkeg="{{$row->kegiatan->name}}">{{$row->user->instansi_name}}</td>
                            <td>{{$row->program->name}}</td>
                            <td>{{$row->kegiatan->name}}</td>
                            <td>{{!empty($row->pagu_kegiatan_perubahan)?'Rp '.number_format($row->pagu_kegiatan_perubahan, 0, '.', '.') : 'Rp '.number_format($row->pagu_kegiatan, 0, '.', '.') }}</td>
                            <td><button type="button" class="btn btn-primary btn-xs comment" data-toggle="modal" data-target="#myModal">COMMENT</button></td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.full.js"></script>
<script type="text/javascript" src="{{ asset('js/jquery.form-validator.min.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $( ".dropdown" ).select2({
        theme: "bootstrap"
    });
    @if (session('status'))
      setTimeout(function(){
        $('.alert').remove();
      }, 5000);
    @endif
    $.validate({
        form : '.form-horizontal'
    });
    $(document).on('click', '.comment', function () {
      $('input[name="nama_skpd"]').val($(this).parent().closest('tr').find('td').data('nama'));
      $('input[name="skpd"]').val($(this).parent().closest('tr').find('td').data('skpd'));
      $('input[name="program"]').val($(this).parent().closest('tr').find('td').data('program'));
      $('input[name="kegiatan"]').val($(this).parent().closest('tr').find('td').data('kegiatan'));
      $('input[name="str_program"]').val($(this).parent().closest('tr').find('td').data('strpro'));
      $('input[name="str_kegiatan"]').val($(this).parent().closest('tr').find('td').data('strkeg'));
    });
    $("#myModal").on("hidden.bs.modal", function () {
      $('input[name="nama_skpd"]').val(null);
      $('input[name="skpd"]').val(null);
      $('input[name="program"]').val(null);
      $('input[name="kegiatan"]').val(null);
      $('input[name="str_program"]').val(null);
      $('input[name="str_kegiatan"]').val(null);
    });
    $('#submit').on('submit', function (e) {
        e.preventDefault();
        var skpd = $('input[name="skpd"]').val();
        var program = $('input[name="program"]').val();
        var kegiatan = $('input[name="kegiatan"]').val();
        var name = $('input[name="name"]').val();
        var email = $('input[name="email"]').val();
        var phone = $('input[name="phone"]').val();
        var comment = $('textarea[name="comment"]').val();
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            type: "POST",
            url: "{{ url('sendComment') }}",
            data: {_token: CSRF_TOKEN, skpd: skpd, program: program, kegiatan: kegiatan, name: name, email: email, phone: phone, comment: comment},
            dataType: 'json',
            success: function( data ) {
                $("#ajaxResponse").append(data.msg);
                $('#myModal').modal('hide');
            }
        }).done(function () {
          setTimeout(function(){
            $('.alert').remove();
          }, 5000);
        });
    });
  });
</script>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form method="POST" class="form-horizontal" id="submit" action="{{ url('sendComment') }}">
      {{ csrf_field() }}
      <input type="hidden" name="skpd" value="">
      <input type="hidden" name="program" value="">
      <input type="hidden" name="kegiatan" value="">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Usulan</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="name" class="col-sm-3 control-label">Program</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="str_program" value="" disabled="">
            </div>
          </div>
          <div class="form-group">
            <label for="name" class="col-sm-3 control-label">Kegiatan</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="str_kegiatan" value="" disabled="">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">SKPD</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="nama_skpd" value="" disabled="">
            </div>
          </div>
          <div class="form-group">
            <label for="name" class="col-sm-3 control-label">Nama</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="name" id="name" autocomplete="off" data-validation="required" data-validation-error-msg="The Nama field is required..">
            </div>
          </div>
          <div class="form-group">
            <label for="email" class="col-sm-3 control-label">Email</label>
            <div class="col-sm-8">
              <input type="email" class="form-control" name="email" id="email" autocomplete="off" data-validation="required" data-validation-error-msg="The Email field is required..">
            </div>
          </div>
          <div class="form-group">
            <label for="phone" class="col-sm-3 control-label">No Handphone</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="phone" id="phone" autocomplete="off" data-validation="required" data-validation-error-msg="The No Handphone field is required..">
            </div>
          </div>
          <div class="form-group">
            <label for="name" class="col-sm-3 control-label">Komentar</label>
            <div class="col-sm-8">
              <textarea class="form-control" rows="3" name="comment" data-validation="required"></textarea>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
          <button type="submit" class="btn btn-primary">Kirim</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
